module.exports = {
    wordFrequency: function(doc, word){
        const docWords = this.stringToWordsArray(doc)

        const count = docWords.reduce((accumulator, value) => {
            if (value.toLowerCase() === word.toLowerCase()){
                return accumulator+1
            }

            return accumulator
        },0)

        return count.toString()
    },

    wordSentences: function(doc, word){
        let sentences = []
        let sentence = ""
        const endOfSentence = [".", ":", ";", "\n"]
        let startSentence = false
        let endSentence = false

        for (let letter of doc){
            if (/[A-Z]/.test(letter) && startSentence === false){
                sentence = ""
                sentence += letter
                startSentence = true
                endSentence = false
            }else if (endOfSentence.includes(letter) && startSentence === true){
                sentence += letter
                if (sentence.toUpperCase().includes(word.toUpperCase())){
                    sentences.push(sentence)
                }
                sentence = ""
                endSentence = true
                startSentence = false
            }else if (startSentence === true && endSentence === false) {
                sentence += letter
            }
        }

        return sentences
    },

    topWords: function(doc, count, minWordLength){
        const docWords = this.stringToWordsArray(doc)
        let done = false

        let occurrences = docWords.reduce((accumulator, value) => {
            let accumulatorCurrentValue = (accumulator[value.toLowerCase()] || 0)

            if (value.length >= minWordLength) {
                accumulatorCurrentValue += 1
                return {...accumulator, [value.toLowerCase()]: accumulatorCurrentValue}
            }else {
                return {...accumulator}
            }
        }, {})

        let topWords = Object.keys(occurrences)
        const occurrencesValues = Object.values(occurrences)

        while (!done) {
            done = true;
            for (let i = 1; i < occurrencesValues.length; i += 1) {
                if (occurrencesValues[i - 1] < occurrencesValues[i]) {
                    done = false;
                    let tmp = occurrencesValues[i - 1];
                    let topWordAux = topWords[i -1]
                    occurrencesValues[i - 1] = occurrencesValues[i];
                    topWords[i-1] = topWords[i]
                    occurrencesValues[i] = tmp;
                    topWords[i] = topWordAux
                }
            }
        }

        return topWords.slice(0, count)
    },

    stringToWordsArray: function(text) {
        return text.match(/[a-zÀ-ú]+/gmui);
    }
}

